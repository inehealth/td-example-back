package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"bitbucket.org/inehealth/td-example-back/configuration"

	"bitbucket.org/inehealth/td-example-back/app"
	"github.com/Sirupsen/logrus"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

//APIServer representation of the global server
type APIServer struct {
	config  *configuration.Configuration
	router  *mux.Router
	negroni *negroni.Negroni
	logger  *logrus.Logger
	Version string
}

type program struct {
	config *configuration.Configuration
}

func main() {

	prg := &program{}

	configFlag := flag.String("configFile", "/etc/td-example-back/config.json", "Path where config file is found")
	flag.Parse()

	var err error

	prg.config, err = configuration.NewConfiguration(*configFlag)
	if err != nil {
		fmt.Printf("Error!!!  %s\n", err)
		os.Exit(1)
	}

	api := APIServer{config: prg.config, logger: logrus.New(), Version: "0.1"}

	//application router
	api.router = app.NewRouter(api.config, api.logger)

	api.negroni = negroni.Classic()
	api.negroni.UseHandler(api.router)
	api.logger.WithFields(logrus.Fields{"Port": api.config.APIPort}).Info("Starting IDONIA Media Server API...")

	addr := fmt.Sprintf(":%d", api.config.APIPort)
	l := log.New(os.Stdout, "[negroni] ", 0)
	l.Printf("listening on %s", addr)
	l.Fatal(http.ListenAndServe(addr, api.negroni))
	if err != nil {
		fmt.Printf("ERROR initializing server: %s\n\n", err)
		os.Exit(-1)
	}

}
