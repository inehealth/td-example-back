package main

import (
	_ "flag"
	"fmt"
	"math/rand"
	_ "os"
	"time"

	"bitbucket.org/inehealth/td-example-back/app/model"
	"bitbucket.org/inehealth/td-example-back/configuration"
	_ "bitbucket.org/inehealth/td-example-back/util"
	"github.com/icrowley/fake"
	"github.com/jackc/pgx"
)

type populateProgram struct {
	config *configuration.Configuration
}

//func main() {
//	fake.SetLang("en")
//
//	var err error
//	prg := &populateProgram{}
//	configFlag := flag.String("configFile", "/etc/td-example-back/config.json", "Path where config file is found")
//	flag.Parse()
//	prg.config, err = configuration.NewConfiguration(*configFlag)
//
//	if err != nil {
//		fmt.Printf("Error!!!  %s\n", err)
//		os.Exit(1)
//	}
//
//	connectionPool, err := util.NewDbConnFromConfig(prg.config)
//
//	if err != nil {
//		fmt.Printf("ERROR Connecting to DB!!!  %s\n", err)
//		os.Exit(1)
//	}
//
//	numRows, err := populateDataBase(connectionPool, 50)
//	if err != nil {
//		fmt.Printf("ERROR Populating DB!!!  %s\n", err)
//		os.Exit(1)
//	}
//
//	fmt.Printf("\n DATABASE POPULATED -> %d tasks created\n\n", numRows)
//
//	os.Exit(0)
//}

func populateDataBase(cnx *pgx.ConnPool, numTasks int) (int, error) {
	tasks := getData(numTasks)

	var task model.TaskDetail
	var rows = make([][]interface{}, numTasks)
	for i := 0; i < numTasks; i++ {
		task = (*tasks)[i]
		rows[i] = []interface{}{
			fmt.Sprintf("%s (%d)", task.Title, i),
			task.DateTime,
			task.DateTimeCompleted,
			task.Priority,
			task.Contents,
		}
	}

	return cnx.CopyFrom(
		pgx.Identifier{"task"},
		[]string{"title", "datetime", "datetime_completed", "priority", "contents"},
		pgx.CopyFromRows(rows),
	)
}

func getData(numTasks int) *[]model.TaskDetail {
	var tasks = make([]model.TaskDetail, numTasks)
	for i := 0; i < numTasks; i++ {
		tasks[i] = model.TaskDetail{
			Task: model.Task{
				Title:             fake.WordsN(2 + rand.Intn(5)),
				DateTime:          randate(),
				DateTimeCompleted: randCompleted(),
				Priority:          randPriority(),
			},
			Contents: fake.Paragraphs(),
		}
	}

	return &tasks
}

func randate() time.Time {
	min := time.Date(2017, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	max := time.Now().Unix()
	delta := max - min

	sec := rand.Int63n(delta) + min
	return time.Unix(sec, 0)
}

func randCompleted() *time.Time {
	if rand.Intn(101) < 33 {
		randDateTime := randate()
		return &randDateTime
	}
	return nil
}

func randPriority() model.Priority {
	var priorities = []model.Priority{
		model.PriorityMin,
		model.PriorityLow,
		model.PriorityLow,
		model.PriorityMed,
		model.PriorityMed,
		model.PriorityMed,
		model.PriorityMed,
		model.PriorityHigh,
		model.PriorityHigh,
		model.PriorityMax,
	}

	return priorities[rand.Intn(len(priorities))]
}
