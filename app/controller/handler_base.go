package controller

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"net/http"
	"reflect"
	"regexp"
	"strconv"
	"strings"

	"bitbucket.org/inehealth/td-example-back/configuration"
	"bitbucket.org/inehealth/td-example-back/util"

	"github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
	"github.com/jackc/pgx"
)

var connectionPool *pgx.ConnPool

//HandlerBase common helper functions for the handlers
type HandlerBase struct {
	conf   *configuration.Configuration
	Routes map[HandlerRouterMethod]func(http.ResponseWriter, *http.Request)
	vars   map[string]string
	logger *logrus.Logger
}

//HandlerRouterMethod particular struct for endpoints definitions
type HandlerRouterMethod struct {
	Endpoint string
	Method   string
}

//ErrorResponse representation for a error response
type ErrorResponse struct {
	Error string `json:"error"`
	Code  int    `json:"code"`
}

func (hb *HandlerBase) initializeRequest(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Authorization, Origin, Content-Type, X-Auth-Token")

	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte{})
		return
	}

	if len(hb.vars) == 0 {
		hb.vars = mux.Vars(r)
		r.ParseMultipartForm(0)
	}

	return
}

func (hb HandlerBase) jsonResponse(response interface{}, w http.ResponseWriter, statusCode int) {
	json, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		hb.logger.Error("JSON RESPONSE ERROR: %s", err.Error())
		return
	}
	if statusCode <= 0 {
		statusCode = http.StatusOK
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Content-Length", strconv.Itoa(len(json)))
	w.WriteHeader(statusCode)
	w.Write(json)
}

func (hb HandlerBase) xmlResponse(response interface{}, w http.ResponseWriter, statusCode int, selfClosingTags []string) {
	xml, err := xml.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var prefix string
	refType := reflect.ValueOf(response).Elem()
	refField := refType.FieldByName("XmlVersion")
	if refField.IsValid() {
		prefix += refField.String() + "\n"
	}
	refField = refType.FieldByName("DocType")
	if refField.IsValid() {
		prefix += refField.String() + "\n"
	}
	if len(selfClosingTags) > 0 {
		r := regexp.MustCompile(fmt.Sprintf("></(%s)>", strings.Join(selfClosingTags, "|")))
		xml = []byte(r.ReplaceAllString(string(xml), "/>"))
	}
	if len(prefix) > 0 {
		xml = append([]byte(prefix), xml...)
	}
	if statusCode <= 0 {
		statusCode = http.StatusOK
	}

	w.Header().Set("Content-Type", "application/xml")
	w.WriteHeader(statusCode)
	w.Write(xml)
}

func (hb HandlerBase) getDbConn(w http.ResponseWriter) *pgx.ConnPool {
	if connectionPool == nil {
		var err error
		connectionPool, err = util.NewDbConnFromConfig(hb.conf)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			hb.logger.Error("ERROR Connecting to DB: %s", err.Error())
			panic(err)
		}
	}
	return connectionPool
}
