package controller

import (
	"net/http"

	"bitbucket.org/inehealth/td-example-back/configuration"
	"bitbucket.org/inehealth/td-example-back/util"

	"github.com/Sirupsen/logrus"
)

//CrossDomainHandler handler for the api router
type CrossDomainHandler struct {
	HandlerBase
}

//NewCrossDomainHandler Definition for the cross domain handler
func NewCrossDomainHandler(conf *configuration.Configuration, logger *logrus.Logger) *CrossDomainHandler {

	handler := CrossDomainHandler{}
	handler.conf = conf
	handler.logger = logger

	return &handler
}

//GetCrossDomainPolicy Controller
func (handler CrossDomainHandler) GetCrossDomainPolicy(w http.ResponseWriter, r *http.Request) {
	cd := util.NewCrossDomain(handler.conf)
	handler.xmlResponse(cd, w, 200, []string{"site-control", "allow-access-from", "allow-http-request-headers-from"})
}
