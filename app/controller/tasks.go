package controller

import (
	"net/http"
	"strconv"

	"bitbucket.org/inehealth/td-example-back/app/repository"
	"bitbucket.org/inehealth/td-example-back/app/service"
	"bitbucket.org/inehealth/td-example-back/configuration"

	"github.com/Sirupsen/logrus"
	"github.com/jackc/pgx"
	"time"
)

//TaskHandler doc
type TaskHandler struct {
	HandlerBase
}

//NewTasksHandler doc
func NewTasksHandler(conf *configuration.Configuration, logger *logrus.Logger) *TaskHandler {

	handler := TaskHandler{}
	handler.conf = conf
	handler.logger = logger

	return &handler
}

//ListAll doc
func (handler TaskHandler) ListAll(w http.ResponseWriter, r *http.Request) {

	handler.initializeRequest(w, r)
	db := handler.getDbConn(w)
	tasks, err := repository.ListTask(db)

	if err == pgx.ErrNoRows {
		w.WriteHeader(http.StatusNoContent)
		return
	} else if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		handler.logger.Error(err.Error())
		return
	}

	handler.jsonResponse(tasks, w, http.StatusOK)
}

//AddTask doc
func (handler TaskHandler) AddTask(w http.ResponseWriter, r *http.Request) {

	handler.initializeRequest(w, r)
	db := handler.getDbConn(w)
	taskDetail, err := service.GetTaskDataFromRq(r)
	if err != nil {
		handler.jsonResponse(&ErrorResponse{err.Error(), 0}, w, http.StatusBadRequest)
		return
	}

	taskID, err := repository.AddTask(db, taskDetail)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		handler.logger.Error(err.Error())
		return
	}

	handler.jsonResponse(map[string]int64{"task_id": taskID}, w, http.StatusCreated)
}

//ViewTask doc
func (handler TaskHandler) ViewTask(w http.ResponseWriter, r *http.Request) {

	handler.initializeRequest(w, r)
	db := handler.getDbConn(w)
	taskDetail, err := repository.FindTaskByIDOrURL(db, handler.vars["url"])

	if err == pgx.ErrNoRows {
		w.WriteHeader(http.StatusNotFound)
		return
	} else if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		handler.logger.Error(err.Error())
		return
	}

	handler.jsonResponse(taskDetail, w, http.StatusOK)
}

//UpdateTask doc
func (handler TaskHandler) UpdateTask(w http.ResponseWriter, r *http.Request) {

	handler.initializeRequest(w, r)
	taskID, err := strconv.Atoi(handler.vars["taskID"])
	if err != nil || taskID <= 0 {
		handler.jsonResponse(&ErrorResponse{err.Error(), 0}, w, http.StatusBadRequest)
		return
	}

	db := handler.getDbConn(w)
	taskDetail, err := service.GetTaskDataFromRq(r)
	if err != nil {
		handler.jsonResponse(&ErrorResponse{err.Error(), 0}, w, http.StatusBadRequest)
		return
	}
	if taskDetail.Completed == true && taskDetail.DateTimeCompleted == nil {
		now := time.Now()
		taskDetail.DateTimeCompleted = &now
	} else if taskDetail.Completed == false {
		taskDetail.DateTimeCompleted = nil
	}
	found, err := repository.UpdateTask(db, taskID, taskDetail)
	if !found {
		w.WriteHeader(http.StatusNotFound)
		return
	} else if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		handler.logger.Error(err.Error())
		return
	}

	w.WriteHeader(http.StatusOK)
}

//DeleteTask doc
func (handler TaskHandler) DeleteTask(w http.ResponseWriter, r *http.Request) {

	handler.initializeRequest(w, r)
	taskID, err := strconv.Atoi(handler.vars["taskID"])
	if err != nil || taskID <= 0 {
		handler.jsonResponse(&ErrorResponse{err.Error(), taskID}, w, http.StatusBadRequest)
		return
	}

	db := handler.getDbConn(w)
	wasDeleted, err := repository.DeleteTask(db, taskID)

	if !wasDeleted {
		w.WriteHeader(http.StatusNotFound)
		return
	} else if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		handler.logger.Error(err.Error())
		return
	}

	w.WriteHeader(http.StatusOK)
}
