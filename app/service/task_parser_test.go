package service

import (
	"net/http"
	"os"
	"reflect"
	"strings"
	"testing"

	"bitbucket.org/inehealth/td-example-back/app/model"
)

var dataProvider = []struct {
	body       string           //input
	taskDetail model.TaskDetail //expected
}{
	{"invalidJson", model.TaskDetail{}},
	{"{}", model.TaskDetail{}},
	{"{\"title\": \"myTitle\"}", model.TaskDetail{Task: model.Task{Title: "myTitle"}}},
}

func TestMain(m *testing.M) {
	// call flag.Parse() here if TestMain uses flags
	os.Exit(m.Run())
}

//TestUnitGetTaskDataFromRq doc
func TestUnitGetTaskDataFromRq(t *testing.T) {

	for _, data := range dataProvider {
		r, _ := http.NewRequest("", "", strings.NewReader(data.body))
		parsed, _ := GetTaskDataFromRq(r)
		if !reflect.DeepEqual(parsed, data.taskDetail) {
			t.Error("For", data.body, "expected", data.taskDetail, "got", parsed)
		}

	}

}

//BenchmarkGetTaskDataFromRq doc
func BenchmarkGetTaskDataFromRq(b *testing.B) {

	r, _ := http.NewRequest("", "", strings.NewReader("{\"title\": \"myTitle\"}"))
	for n := 0; n < b.N; n++ {
		GetTaskDataFromRq(r)
	}
}
