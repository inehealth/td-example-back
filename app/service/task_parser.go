package service

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/inehealth/td-example-back/app/model"
)

//GetTaskDataFromRq doc
func GetTaskDataFromRq(r *http.Request) (task model.TaskDetail, err error) {

	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&task)
	return

	//defer r.Body.Close()

}
