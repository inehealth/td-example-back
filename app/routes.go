package app

import (
	"net/http"
	"reflect"
	"runtime"
	"strings"

	"bitbucket.org/inehealth/td-example-back/app/controller"
	"bitbucket.org/inehealth/td-example-back/configuration"

	"github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
)

//Route doc
type Route struct {
	Pattern  string
	Function func(http.ResponseWriter, *http.Request)
	Method   string
}

//NewRouter doc
//OPTIONS method always is added
func NewRouter(conf *configuration.Configuration, logger *logrus.Logger) *mux.Router {

	router := mux.NewRouter().StrictSlash(true)

	ch := controller.NewCrossDomainHandler(conf, logger)
	th := controller.NewTasksHandler(conf, logger)

	routes := []Route{

		//CROSSDOMAIN
		{"/crossdomain.xml", ch.GetCrossDomainPolicy, "GET"},

		//TASKS
		{"/api/tasks", th.ListAll, "GET"},
		{"/api/tasks", th.AddTask, "POST"},
		{"/api/tasks/{url}", th.ViewTask, "GET"},
		{"/api/tasks/{taskID}", th.UpdateTask, "PUT"},
		{"/api/tasks/{taskID}", th.DeleteTask, "DELETE"},
	}

	for _, r := range routes {
		router.HandleFunc(r.Pattern, r.Function).Methods(r.Method, "OPTIONS")
		logger.WithFields(logrus.Fields{"Path": r.Pattern, "Handler": getNameOfFunc(r.Function), "Method": r.Method}).Info("- Route Registered")
	}

	return router
}

//getNameOfFunc doc
func getNameOfFunc(f interface{}) string {
	funcName := runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name()
	return funcName[strings.Index(funcName, "("):]
}
