package repository

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/inehealth/td-example-back/app/model"

	"github.com/jackc/pgx"
)

//ListTask return all the task persisted in the DB
func ListTask(db *pgx.ConnPool) (tasks []model.Task, err error) {

	sql := "SELECT task_id, datetime, title, datetime_completed, priority, url FROM task"

	rows, err := db.Query(sql)
	defer rows.Close()

	if err == nil {
		noRows := true
		task := new(model.Task)
		for rows.Next() {
			noRows = false

			err = rows.Scan(
				&task.TaskID,
				&task.DateTime,
				&task.Title,
				&task.DateTimeCompleted,
				&task.Priority,
				&task.URL,
			)
			if err == nil {
				task.Completed = task.DateTimeCompleted != nil && !task.DateTimeCompleted.IsZero()
				tasks = append(tasks, *task)
			}
		}
		if noRows {
			return tasks, pgx.ErrNoRows
		}
	}

	return tasks, err

}

//AddTask create a new Task into DB and return the ID
func AddTask(db *pgx.ConnPool, taskDetail model.TaskDetail) (taskID int64, err error) {

	err = db.QueryRow(
		"INSERT INTO task (title, contents, priority, url) VALUES ($1, $2, $3, $4) RETURNING task_id",
		taskDetail.Title,
		taskDetail.Contents,
		taskDetail.Priority,
		taskDetail.URL,
	).Scan(&taskID)

	return
}

//FindTaskByIDOrURL search by url or ID fields
func FindTaskByIDOrURL(db *pgx.ConnPool, url string) (task model.TaskDetail, err error) {
	sql := "SELECT task_id, datetime, title, datetime_completed, contents, priority, url FROM task WHERE %s"
	taskID, err := strconv.Atoi(url)
	var row *pgx.Row
	if err != nil {
		sql = fmt.Sprintf(sql, "url LIKE $1")
		row = db.QueryRow(sql, url)
	} else {
		sql = fmt.Sprintf(sql, "task_id = $1")
		row = db.QueryRow(sql, taskID)
	}
	err = row.Scan(&task.TaskID, &task.DateTime, &task.Title, &task.DateTimeCompleted, &task.Contents, &task.Priority, &task.URL)

	task.Completed = task.DateTimeCompleted != nil && !task.DateTimeCompleted.IsZero()

	return task, err
}

//UpdateTask update the tasks
func UpdateTask(db *pgx.ConnPool, taskID int, td model.TaskDetail) (wasEdited bool, err error) {
	sql := "UPDATE task SET %s WHERE task_id = $1"

	params := []interface{}{taskID}
	var paramsSQL []string
	addParamToQuery(td.Title, "title", &params, &paramsSQL)
	addParamToQuery(td.Contents, "contents", &params, &paramsSQL)
	addParamToQuery(string(td.Priority), "priority", &params, &paramsSQL)
	params = append(params, td.DateTimeCompleted)
	paramsSQL = append(paramsSQL, fmt.Sprintf("datetime_completed = $%d", len(params)))

	sql = fmt.Sprintf(sql, strings.Join(paramsSQL, ", "))

	ct, err := db.Exec(sql, params...)

	if err != nil {
		return
	}

	wasEdited = ct.RowsAffected() > 0

	return
}

func addParamToQuery(value string, colName string, paramsValues *[]interface{}, paramsSQL *[]string) {
	if len(value) > 0 {
		*paramsValues = append(*paramsValues, value)
		*paramsSQL = append(*paramsSQL, fmt.Sprintf("%s = $%d", colName, len(*paramsValues)))
	}
}

//DeleteTask delete the task
func DeleteTask(db *pgx.ConnPool, taskID int) (wasDeleted bool, err error) {

	sql := "DELETE FROM task WHERE task_id = $1"
	ct, err := db.Exec(sql, taskID)

	if err != nil {
		return
	}

	wasDeleted = ct.RowsAffected() > 0

	return
}
