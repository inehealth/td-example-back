package model

import "errors"

//Priority is the representation of a field of priority's enum
type Priority string

//Static representation of the priority enum
const (
	PriorityMin  = Priority("min")
	PriorityLow  = Priority("low")
	PriorityMed  = Priority("med")
	PriorityHigh = Priority("high")
	PriorityMax  = Priority("max")
)

//GetPriority return a constant from string
func GetPriority(p string) (priority Priority, err error) {

	switch p {
	case "min":
		priority = PriorityMin
	case "low":
		priority = PriorityLow
	case "med":
		priority = PriorityMed
	case "high":
		priority = PriorityHigh
	case "max":
		priority = PriorityMax
	default:
		err = errors.New("Priotity not allowed: " + p)
	}

	return
}
