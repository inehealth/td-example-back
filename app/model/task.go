package model

import "time"

//Task model representation into a struc
type Task struct {
	TaskID            uint32     `json:"task_id"`
	Title             string     `json:"title"`
	DateTime          time.Time  `json:"datetime"`
	Completed         bool       `json:"completed"`
	DateTimeCompleted *time.Time `json:"datetime_completed"`
	Priority          Priority   `json:"priority"`
	URL               string     `json:"url"`
}

//TaskDetail model representation for the view of ONE task
type TaskDetail struct {
	Task
	Contents string `json:"contents"`
}
