package configuration

import (
	"encoding/json"
	"github.com/Sirupsen/logrus"
	"os"
)

var log = logrus.New()

//Configuration global configuration
type Configuration struct {
	DbHost         string
	DbPort         uint16
	DbName         string
	DbUser         string
	DbPass         string
	APIPort        uint16
	APIHost        string
	UIFilesDir     string
	AllowedDomains string
	AllowedPorts   string
}

//NewConfiguration create a new instance for the app configuration
func NewConfiguration(filePath string) (*Configuration, error) {
	var file *os.File
	defer file.Close()
	file, err := os.Open(filePath)
	decoder := json.NewDecoder(file)
	configuration := Configuration{}
	if err == nil {
		err = decoder.Decode(&configuration)
	} else {
		log.WithFields(logrus.Fields{"filepath": filePath}).
			Fatal("[Configuration System] Config file not found or not readable")
	}
	return &configuration, err
}
