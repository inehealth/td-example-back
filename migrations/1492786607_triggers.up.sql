CREATE OR REPLACE FUNCTION trigger_fn_update_url()
  RETURNS TRIGGER
AS
$$
BEGIN
  NEW.url = friendly_url(NEW.title);
  RETURN NEW;
END;
$$
LANGUAGE PLPGSQL STABLE;

CREATE TRIGGER trigger_update_url
BEFORE
INSERT OR UPDATE ON task
FOR EACH ROW EXECUTE PROCEDURE trigger_fn_update_url();
