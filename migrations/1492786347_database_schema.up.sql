CREATE TYPE priority_level AS ENUM
('min',
  'low',
  'med',
  'high',
  'max');

CREATE TABLE task (
  task_id            SERIAL                      NOT NULL PRIMARY KEY,
  datetime           TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
  title              VARCHAR(100)                NOT NULL,
  datetime_completed TIMESTAMP WITHOUT TIME ZONE NULL,
  contents           TEXT,
  priority           priority_level              NOT NULL DEFAULT 'med',
  url                VARCHAR(100)                NOT NULL UNIQUE
);
