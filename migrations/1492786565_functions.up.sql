CREATE EXTENSION unaccent;

CREATE OR REPLACE FUNCTION friendly_url(url TEXT)
  RETURNS TEXT
AS $$
SELECT
  TRIM(BOTH '-' FROM regexp_replace(regexp_replace(unaccent(lower(url)), '[^a-zA-Z0-9_+.]', '-', 'g'), '-+', '-', 'g'));
$$ LANGUAGE SQL;