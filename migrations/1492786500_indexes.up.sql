CREATE INDEX idx_cluster_task
  ON task USING BTREE ((datetime :: DATE) DESC, priority);

CREATE INDEX idx_search_url
  ON task USING BTREE (url, task_id DESC, datetime DESC);
--WHERE datetime_completed IS NULL;

CLUSTER task USING idx_cluster_task;