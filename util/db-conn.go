package util

import (
	"bitbucket.org/inehealth/td-example-back/configuration"
	"github.com/jackc/pgx"
)

//NewDbConnFromConfig returns a new postgres' connection from the pool
func NewDbConnFromConfig(conf *configuration.Configuration) (*pgx.ConnPool, error) {
	connConfig := pgx.ConnConfig{Host: conf.DbHost, Port: conf.DbPort, User: conf.DbUser, Password: conf.DbPass, Database: conf.DbName}
	cnxCnf := pgx.ConnPoolConfig{ConnConfig: connConfig, MaxConnections: 200}
	conn, err := pgx.NewConnPool(cnxCnf)
	return conn, err
}
