package util

import (
	"bitbucket.org/inehealth/td-example-back/configuration"
	"encoding/xml"
)

//CrossDomain is the struct representation for the cross domain
type CrossDomain struct {
	XMLName     xml.Name `xml:"cross-domain-policy"`
	SiteControl struct {
		PermittedPolicies string `xml:"permitted-cross-domain-policies,attr"`
	} `xml:"site-control"`
	AllowAccessFrom struct {
		Domain string `xml:"domain,attr"`
		Ports  string `xml:"to-ports,attr"`
	} `xml:"allow-access-from"`
	AllowHeaders struct {
		Domain  string `xml:"domain,attr"`
		Headers string `xml:"headers,attr"`
	} `xml:"allow-http-request-headers-from"`
	DocType    string `xml:"-"`
	XMLVersion string `xml:"-"`
}

//NewCrossDomain contructs the representation of the cross domain into a xml
func NewCrossDomain(config *configuration.Configuration) *CrossDomain {

	cd := new(CrossDomain)
	cd.XMLVersion = "<?xml version=\"1.0\"?>"
	cd.DocType = "<!DOCTYPE cross-domain-policy SYSTEM \"http://www.macromedia.com/xml/dtds/cross-domain-policy.dtd\">"
	cd.SiteControl.PermittedPolicies = "all"
	cd.AllowAccessFrom.Domain = config.AllowedDomains
	cd.AllowAccessFrom.Ports = config.AllowedPorts
	cd.AllowHeaders.Domain = config.AllowedDomains
	cd.AllowHeaders.Headers = "*"

	return cd
}
